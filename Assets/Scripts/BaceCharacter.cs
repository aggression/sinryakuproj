﻿using UnityEngine;
using System.Collections;

public class BaceCharacter : MonoBehaviour {

	protected float _speed;	//スピード
	protected float _posX;	//X軸のポジション
	protected float _posY;	//Y軸のポジション
	protected float _posZ;  //Z軸のポジション
	protected int _state;	//状態
	protected Vector3 _trackingPos;	//追跡する相手のポジション
	protected float _trackingSpeed;

	protected GameObject _player;
	protected GameObject _hiroin;

	protected const int NORMAL_MOVE = 0;	//通常状態
	protected const int TRACKING_MOVE = 1;	//追跡状態

	private const int DESTROY_POS = -10;

	public void Start(){
		_player = GameObject.Find("Player");
		_hiroin = GameObject.Find ("Hiroin");
		_state = NORMAL_MOVE;
	}
	public virtual void Destroy() {
		if(this.transform.localPosition.z < DESTROY_POS){
			Destroy (this.gameObject);
		}
	}

	public virtual void Scroll(){
		_posZ = this.transform.position.z - _speed;
	}

	public virtual void OnTriggerEnter(Collider target){
		if (target.gameObject.tag == "Attack") {
			Destroy(this.gameObject);
		}else if(target.gameObject.tag == "Damage"){
			//プレイヤーのライフを減らす
			GameObject _player = GameObject.Find("Player");
			_player.GetComponent<Animator> ().SetInteger("animation", 1);
			_player.transform.root.GetComponent<Life>().ReceiveDamage();
			Destroy(this.gameObject);
			//_player.GetComponent<Animator> ().SetInteger("animation", 0);

		}
	}

}