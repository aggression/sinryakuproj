﻿using UnityEngine;
using System.Collections;

public class StraightEnemy : BaceCharacter {
	private enum State{
		Straight,
		Turn,
		Bump
	}
	private float _degrees;
	private float _currentDeg;
	private const float EASING = 0.03f;
	private const float LIMIT_RAD = 0.1f;

	// Use this for initialization
	void Start () {
		_speed = Random.Range (0.05f, 0.2f);
		_trackingSpeed = 3;
		_degrees = 0;
		_currentDeg = 0;
		_state = (int)State.Straight;
		base.Start ();
	}

	// Update is called once per frame
	void Update () {
		if(GameManager.GetPauseState() == false){
			base.Destroy();
			switch(_state){
				//まっすぐ進む処理
				case (int)State.Straight:
					MoveStraight();
				break;
				//回転する処理
				case (int)State.Turn:
					TurnState();
				break;
				//ヒロインに向かって進む処理
				case (int)State.Bump:
					BumpState();
				break;
			}
		}
	}

	//まっすぐ進むための処理
	private void MoveStraight(){
		transform.position -= new Vector3(0, 0, _speed);
		if(_player.transform.position.z >= this.transform.position.z){
			_state = (int)State.Turn;
			InitializeTurnState();
		}

	}

	//回転処理に必要な変数の初期化
	private void InitializeTurnState(){
		float x = _hiroin.transform.position.x - transform.position.x;
		float y = _hiroin.transform.position.z - transform.position.z;
		_degrees = Mathf.Rad2Deg * Mathf.Atan2(x, y);
		int num = _degrees < 0 ? -1 : 1;
		_degrees = (Mathf.Abs(_degrees) - 180) * num;
		_currentDeg = transform.eulerAngles.y;
		Debug.Log("_currentDeg : " + _currentDeg + "   _degrees : " + _degrees);
	}

	//回転する処理
	private void TurnState(){
		_currentDeg += (_degrees - _currentDeg) * EASING;
		transform.rotation = Quaternion.Euler(0, _currentDeg, 0);

		//目標の角度に近づいたら状態を変化
		if(_degrees - _currentDeg < LIMIT_RAD){
			_state = (int)State.Bump;
		}
	}

	//ヒロインに向かって進む処理
	private void BumpState(){
		transform.Translate(transform.forward * -1 * _speed);
	}

	void MoveChange(){
		switch(_state){
		case NORMAL_MOVE:  //通常状態
			this.transform.position = new Vector3 (this.transform.localPosition.x, this.transform.localPosition.y, _posZ);
			break;

		case TRACKING_MOVE:	//追撃状態
			//this.transform.LookAt(_hiroin.tranform);
			this.transform.position += _trackingPos * _trackingSpeed * Time.deltaTime;
			break;

		}
	}
}