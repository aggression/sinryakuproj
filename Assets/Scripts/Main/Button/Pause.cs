﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {
	private bool _pause;
	public GameObject _panel;
	public GameObject _canvas;

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {

	}

	public void PauseJump(){
		_panel.SetActive (true);
		GameManager.EnabledPause();
	}

	public void BackToGame(){
		_panel.SetActive(false);
		GameManager.NotEnabledPause();
	}
}
