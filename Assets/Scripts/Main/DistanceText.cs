﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DistanceText : MonoBehaviour {
	private float _time;
	private int _textNum;
	private int _distans;
	private GameObject _textObj;

	private int[] _meterInsTime;
	private const int DIS = 10;
	private const int MAX_DISTANS = 6;

	//100metter10byou

	// Use this for initialization
	void Start () {
		_time = 0;
		_textNum = 1;
		_meterInsTime = new int[]{10, 20, 30, 40, 50};
		_distans = DIS;
	}

	// Update is called once per frame
	void Update () {
		if(GameManager.GetPauseState() == false){
			_time += Time.deltaTime * 1;

			if(_time >= _distans){

				GameObject meterObj = Instantiate(Resources.Load("Prefabs/DistanceText")as GameObject);
				meterObj.transform.GetComponent<TextMesh> ().text = "----後" + (_distans * (MAX_DISTANS - _textNum)) as string + "m----";
				_textObj = meterObj;
				_textNum += 1;
				_time = 0;

			}else if(MAX_DISTANS - _textNum < 0){
				SceneManager.LoadScene("Clear");  //クリアシーンへジャンプ
			}

			if(_textObj != null){
				Vector3 objVec = _textObj.transform.position;
				objVec.z -= this.transform.GetComponent<Corridor> ()._scrollSpeed;
				_textObj.transform.position = objVec;
			}
		}


	}
}
