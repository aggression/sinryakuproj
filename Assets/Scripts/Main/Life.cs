using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Life : MonoBehaviour {
	public GameObject _lifeObj;
	public GameObject OriginalPosition;
	private GameObject[] _lifeClone;
	public GameObject _canvasObject;
	private int _currentLife;
	private bool _isDead;
	public bool IsDead { get{ return _isDead;} set{ _isDead = value;}}

	private const int MAX_LIFE = 5;
	// Use this for initialization

	//定数
	private const int INTERVAL_Y = 35;

	void Start () {
		_lifeClone = new GameObject[MAX_LIFE];
		LifeNum (MAX_LIFE);
		_currentLife = MAX_LIFE;
		_isDead = false;
	}

	// Update is called once per frame
	void Update () {
		if(_currentLife <= 0){
			SceneJump ();
		}
	}

	void LifeNum(int life){
		for(int i = 0; i < life; i++){
			_lifeClone[i] = (GameObject)Instantiate (_lifeObj, Vector3.zero, this.transform.rotation);
			_lifeClone[i].transform.SetParent (_canvasObject.transform, false);

			//位置調節
			Vector3 pos = OriginalPosition.transform.localPosition;
			pos.y += (i * INTERVAL_Y);
			pos.z = 0;
			_lifeClone[i].transform.localPosition = pos;

		}
	}

	//ダメージを受けた時の処理
	public void ReceiveDamage(){
		Debug.Log(_currentLife);
		//自分のライフを減らす
		_currentLife--;
		if(_currentLife < 0){
			_currentLife = 0;
			_isDead = true;
		}

		//表示しているライフの表示を変える
		for(int i = _currentLife; i < _lifeClone.Length; i++){
			if(_lifeClone[i] != null){
				Destroy(_lifeClone[i].gameObject);
			}
		}
	}

	public void SceneJump(){
		SceneManager.LoadScene("Over");
	}
}