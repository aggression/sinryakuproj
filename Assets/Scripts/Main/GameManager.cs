﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	private bool _pause = false;

	private static GameManager _singleton;
	private static GameObject _singletonObject;
	private static GameManager GetInstance(){
		if(_singleton == null){
			_singletonObject = new GameObject();
			_singletonObject.AddComponent<GameManager>();
			_singleton = _singletonObject.GetComponent<GameManager>();
		}
		return _singleton;
	}

	public static void EnabledPause(){
		GetInstance()._pause = true;
	}

	public static void NotEnabledPause(){
		GetInstance()._pause = false;
	}

	public static bool GetPauseState(){
		return GetInstance()._pause;
	}

}
