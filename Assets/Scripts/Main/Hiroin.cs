﻿using UnityEngine;
using System.Collections;

public class Hiroin : MonoBehaviour {

	public float _speed;
	public GameObject _player;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		float distans = Mathf.Abs(_player.transform.position.x - this.transform.position.x); 

		float posX = Mathf.Lerp (this.transform.position.x, _player.transform.position.x, _speed);
		this.transform.position = new Vector3 (posX, this.transform.position.y, this.transform.position.z);

	}
}
