using UnityEngine;
using System.Collections;

public class Corridor : MonoBehaviour {
	public float _scrollSpeed;  //画面がスクロールするスピード
	public float WallScrollSpeed;

	//廊下の変数
	private GameObject _instanceCorridor;
	private float _colliderStartZ;
	private float _colliderLimitZ;
	private GameObject _corridorOne;
	private GameObject _corridorTwo;
	private GameObject _corridorThree;

	//壁の変数
	private GameObject _instanceRightWall;
	private GameObject _instanceLeftWall;
	private float _wallY;
	private float _wallRightX, _wallLeftX;
	private GameObject[,] _wall = new GameObject[3,2];
	private float _wallStartZ;



	// Use this for initialization
	void Start () {
		InitializeWallMember();
		InitializeColliderMember();
	}

	// Update is called once per frame
	void Update () {
		if(GameManager.GetPauseState() == false){
			CorridorUpdate(_corridorOne);
			CorridorUpdate(_corridorTwo);
			CorridorUpdate(_corridorThree);
			for(int i = 0; i < _wall.GetLength(0); i++){
				WallUpdate(_wall[i, 0]);
				WallUpdate(_wall[i, 1]);
			}
		}
	}

	void InitializeColliderMember(){
		_instanceCorridor = Resources.Load("Rouka") as GameObject;
		//位置決め
		_colliderStartZ = _instanceCorridor.transform.localScale.z / 2;
		float centerZ = 0;
		float backZ = -_instanceCorridor.transform.localScale.z / 2;
		_colliderLimitZ = -_instanceCorridor.transform.localScale.z;
		//生成
		_corridorOne = (GameObject)Instantiate(_instanceCorridor, new Vector3(0, 0, _colliderStartZ), Quaternion.identity);
		_corridorTwo = (GameObject)Instantiate(_instanceCorridor, new Vector3(0, 0, centerZ), Quaternion.identity);
		_corridorThree = (GameObject)Instantiate(_instanceCorridor, new Vector3(0, 0, backZ), Quaternion.identity);
	}

	void InitializeWallMember(){
		//リソースからプレファブを取得
		_instanceRightWall = Resources.Load("Prefabs/RightWall") as GameObject;
		_instanceLeftWall = Resources.Load("Prefabs/LeftWall") as GameObject;
		//壁の位置決め
		_wallStartZ = _instanceRightWall.transform.localScale.z;
		_wallRightX = _instanceRightWall.transform.position.x;
		_wallLeftX = _instanceLeftWall.transform.position.x;
		_wallY = _instanceLeftWall.transform.position.y;
		//壁を生成
		for(int i = 0; i < 3; i++){
			float wallZ = _wallStartZ - _wallStartZ * i;
			_wall[i, 0] = (GameObject)Instantiate(_instanceRightWall, new Vector3(_wallRightX, _wallY, wallZ), _instanceRightWall.transform.rotation);
			_wall[i, 1] = (GameObject)Instantiate(_instanceLeftWall, new Vector3(_wallLeftX, _wallY, wallZ), _instanceLeftWall.transform.rotation);
		}
	}

	private void CorridorUpdate(GameObject corridor){
		//現在位置から速度をマイナス
		Vector3 pos = corridor.transform.position;
		pos -= new Vector3(0, 0, _scrollSpeed);
		//限界値であったら、最初の位置に戻す
		if(pos.z <= _colliderLimitZ){
			pos.z = _colliderStartZ;
		}
		corridor.transform.position = pos;
	}

	private void WallUpdate(GameObject wall){
		//現在位置から速度をマイナス
		Vector3 pos = wall.transform.position;
		pos -= new Vector3(0, 0, WallScrollSpeed);
		//限界値であったら、最初の位置に戻す
		if(pos.z <= -_wallStartZ){
			pos.z = _wallStartZ;
		}
		wall.transform.position = pos;
	}
}
