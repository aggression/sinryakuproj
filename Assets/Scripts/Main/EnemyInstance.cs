using UnityEngine;
using System.Collections;

public class EnemyInstance : MonoBehaviour {
	private enum EnemyType{
		enemy_1,
		enemy_2
	}
	private EnemyType _enemyType;
	private float _time;
	private int _randomEnemy;
	private float _posZ;

	private int ENEMY_INSTANCE_TIME = 2;	//enemyが生成する時間

	//-----スクリーンの大きさ----//
	private Vector2 _scrrenMin;
	private Vector2 _scrrenMax;

	//----生成可能か判断する変数-----
	public bool AbleInstance;

	// Use this for initialization
	void Start () {
		_scrrenMin = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		_scrrenMax = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
	}

	// Update is called once per frame
	void Update () {
		if(GameManager.GetPauseState() == false && AbleInstanceJudge()){
			_time += Time.deltaTime * 1;

			if(ENEMY_INSTANCE_TIME < _time){	//enemyを生成する
				//どの敵が出現するかランダムで判定し、生成
				int enemyNum = Random.Range (0, System.Enum.GetNames(typeof(EnemyType)).Length);
				_enemyType = (EnemyType)System.Enum.ToObject(typeof(EnemyType) , enemyNum);
				InstanceOfEnemyObject(_enemyType);

				_time = 0;  //初期化　
			}
		}

		//デバッグ用メソッドの呼び出し
		DebugMethod();
	}

	//敵を出現させるメソッド
	void InstanceOfEnemyObject(EnemyType type){
		//生成
		GameObject enemyObj = Instantiate (Resources.Load("Prefabs/" + System.Enum.GetName(typeof(EnemyType), type)) as GameObject);

		//敵の位置を調節
		_posZ =  Random.Range (_scrrenMin.x + 1.2f, _scrrenMax.x - 1.2f);
		this.transform.position = new Vector3 (_posZ, this.transform.position.y, this.transform.position.z);  //生成位置の変更
		enemyObj.transform.position = this.transform.position;
	}

	//出現可能か調べる(デバッグ用)
	bool AbleInstanceJudge(){
		if(Input.GetKeyDown(KeyCode.A)){
			AbleInstance = !AbleInstance;
			Debug.Log("ableInstanceJudge now state is : " + AbleInstance);
		}
		return AbleInstance;
	}

	//一度だけクチモトさんを出現させる（デバッグ用）
	void InstanceStraightEnemyOneShot(){
		//クチモトのKを押したら召喚！
		if(Input.GetKeyDown(KeyCode.K)){
			var type = EnemyType.enemy_2;
			InstanceOfEnemyObject(type);
		}
	}

	//一度だけフトガミさんを出現させる（デバッグ用）
	void InstanceWaveEnemyOneShot(){
		//フトガミのFを押したら召喚！
		if(Input.GetKeyDown(KeyCode.F)){
			var type = EnemyType.enemy_1;
			InstanceOfEnemyObject(type);
		}
	}

	//上二つのまとめメソッド
	void DebugMethod(){
		InstanceStraightEnemyOneShot();
		InstanceWaveEnemyOneShot();
	}


}
