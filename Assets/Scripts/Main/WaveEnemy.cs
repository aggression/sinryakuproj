﻿using UnityEngine;
using System.Collections;

public class WaveEnemy : BaceCharacter {
	private float _frameCount;

	// Use this for initialization
	void Start () {
		_speed = Random.Range (0.05f, 0.2f);
		_trackingSpeed = 3;
		_frameCount = 0;
		base.Start ();
	}

	// Update is called once per frame
	void Update () {
		if(GameManager.GetPauseState() == false){
			base.Destroy();
			base.Scroll ();
			MoveChange ();
			if(_player.transform.position.z >= this.transform.position.z && _state != TRACKING_MOVE){
				_trackingPos = _hiroin.transform.position;
				_trackingPos.Normalize();
				_state = TRACKING_MOVE;
			}
		}
	}

	void MoveChange(){
		switch(_state){
		case NORMAL_MOVE:  //通常状態
			_frameCount += Time.deltaTime * 60;
			this.transform.position = new Vector3( Mathf.Sin(_frameCount * _speed), this.transform.localPosition.y, _posZ);  //ジグザグ移動
			break;

		case TRACKING_MOVE:	//追撃状態
			this.transform.position += _trackingPos * _trackingSpeed * Time.deltaTime;
			break;

		}
	}
}
