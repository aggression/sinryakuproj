using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	private GameObject _originalObject;
	private GameObject _moveObject;
	private Image _originalImage;
	private Image _moveImage;

	private bool _active;

	//-----ジョイスティックで使う変数----
	public float LIMIT_RANGE = 110f;
	private const float ANDROID_RANGE = 2.3f;
	private const float MINUS_SPEED = 0.5f;
	private Vector3 _originPos;
	private Vector3 _currentPos;

	//----動くプレイヤーを制御する変数-----
	private Rigidbody _playerRigidbody;
	public float PlayerSpeed = 0.09f;

	private const float UP_WALL = 15;
	private const float DOWN_WALL = -7;
	private const float LEFT_WALL = -2.3f;
	private const float RIGHT_WALL = 1.9f;

	// Use this for initialization
	void Start () {
		//----JoyStick----
		_active = false;
		_originalObject = GameObject.Find("PlayerCanvas").transform.FindChild("Original").gameObject;
		_originalImage = _originalObject.GetComponent<Image>();
		_moveObject = GameObject.Find("PlayerCanvas").transform.FindChild("Move").gameObject;
		_moveImage = _moveObject.GetComponent<Image>();

		//----Player-----
		_playerRigidbody = GetComponent<Rigidbody>();
		DisAppearJoyStick();

#if UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android){
			LIMIT_RANGE *= ANDROID_RANGE;
			PlayerSpeed /= ANDROID_RANGE;
		}
#endif
	}

	// Update is called once per frame
	void Update () {
		if(GameManager.GetPauseState() == false){
			if(Input.GetMouseButtonDown(0)){
				_active = true;
				InitializeJoystick();
			}

			if(Input.GetMouseButtonUp(0)){
				_active = false;
				DisAppearJoyStick();
			}
		} else {
			_active = false;
			DisAppearJoyStick();
		}

		JoyStickProcessing();
		Clamp ();
	}

	//ジョイスティックの位置を更新するプログラム
	private void InitializeJoystick(){
		_originPos = Input.mousePosition;
		_originalObject.transform.position = _originPos;
		_moveImage.enabled = true;
		_originalImage.enabled = true;
	}

	private void DisAppearJoyStick(){
		_moveImage.enabled = false;
		_originalImage.enabled = false;
		_playerRigidbody.velocity = Vector3.zero;
	}


	//ジョイスティックが動くプログラム
	private void JoyStickProcessing(){
		if(_active){
			_currentPos = Input.mousePosition;

			//表示用操作
			float dx = _originPos.x - _currentPos.x;
			float dy = _originPos.y - _currentPos.y;
			float rad = Mathf.Atan2(dy, dx);
			float limX = Mathf.Cos(rad - 180 * Mathf.Deg2Rad) * LIMIT_RANGE;
			float limY = Mathf.Sin(rad - 180 * Mathf.Deg2Rad) * LIMIT_RANGE;
			_currentPos.x = Mathf.Clamp(_currentPos.x, _originPos.x - Mathf.Abs(limX), _originPos.x + Mathf.Abs(limX));
			_currentPos.y = Mathf.Clamp(_currentPos.y, _originPos.y - Mathf.Abs(limY), _originPos.y + Mathf.Abs(limY));

			_moveObject.transform.position = _currentPos;  //ジョイスティックの位置を変更

			//プレイヤーの位置を変更
			float speedX = _currentPos.x - _originPos.x;
			float speedZ = _currentPos.y - _originPos.y;
			_playerRigidbody.velocity = new Vector3(speedX * MINUS_SPEED, 0, speedZ) * PlayerSpeed;
			transform.localRotation = Quaternion.LookRotation(new Vector3(-speedX, 0, -speedZ));
		}
	}

	void Clamp(){
		Vector3 pos = this.transform.position;
		if (pos.x >= RIGHT_WALL)
			pos.x = RIGHT_WALL;

		if (pos.x <= LEFT_WALL)
			pos.x = LEFT_WALL;

		if (pos.z >= UP_WALL)
			pos.z = UP_WALL;

		if (pos.z <= DOWN_WALL)
			pos.z = DOWN_WALL;

		this.transform.position = pos;

	}
}
